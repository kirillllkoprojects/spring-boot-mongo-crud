package ru.kirilllko.api.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.bind.annotation.*;
import ru.kirilllko.api.model.User;
import ru.kirilllko.api.service.UserService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "Вернуть всех пользователей")
    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @ApiOperation(value = "Вернуть пользоватлея по id")
    @GetMapping(value = "/{userId}")
    public User getUserById(@PathVariable @NotEmpty String userId) {
        return userService.getUserById(userId);
    }

    @ApiOperation(value = "Обновить пользователя")
    @PostMapping
    public User updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @ApiOperation(value = "Удалить пользователя")
    @DeleteMapping(value = "/{userId}")
    public void deleteUser(@PathVariable @NotEmpty String userId) {
        userService.deleteUser(userId);
    }
}