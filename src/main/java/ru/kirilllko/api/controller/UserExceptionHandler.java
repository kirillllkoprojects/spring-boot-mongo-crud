package ru.kirilllko.api.controller;

import org.springframework.validation.Errors;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.kirilllko.api.exception.ExceptionComponent;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@RestController
public class UserExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public Map validationError(final MethodArgumentNotValidException e) {
        return error(Optional.of(e)
                .map(MethodArgumentNotValidException::getBindingResult)
                .map(Errors::getFieldErrors)
                .map(errors -> errors.stream()
                        .map(error -> new ExceptionComponent()
                                .setField(error.getField())
                                .setMessage(error.getDefaultMessage()))
                        .collect(toList()))
                .orElseThrow(() -> new IllegalArgumentException("error is empty")));
    }

    private Map error(Object message) {
        return Collections.singletonMap("error", message);
    }
}
