package ru.kirilllko.api.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.kirilllko.api.model.User;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    User findByUserId(String userId);

    List<User> findAll();

    User save(User user);

    void deleteByUserId(String userId);
}
