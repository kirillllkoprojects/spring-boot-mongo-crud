package ru.kirilllko.api.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kirilllko.api.model.User;

import java.util.List;

/**
 * Created by kirillzubkov on 17/05/2017.
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User getUserById(String userId) {
        return userRepository.findByUserId(userId);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public User updateUser(User user) {
        try {
            return userRepository.save(user);
        }catch (Exception e){
            throw new RuntimeException("error updating user " + user.getUserId() + " ,error message: " + e.getMessage());
        }
    }

    @Override
    @Transactional
    public void deleteUser(String userId) {
        try {
            userRepository.deleteByUserId(userId);
        }catch (Exception e){
            throw new RuntimeException("error deleting user " + userId + " ,error message: " + e.getMessage());
        }

    }
}