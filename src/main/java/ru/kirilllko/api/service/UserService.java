package ru.kirilllko.api.service;

import ru.kirilllko.api.model.User;

import java.util.List;

/**
 * Created by kirillzubkov on 17/05/2017.
 */
public interface UserService {

    User getUserById(String userId);

    List<User> getUsers();

    User updateUser(User user);

    void deleteUser(String userId);
}