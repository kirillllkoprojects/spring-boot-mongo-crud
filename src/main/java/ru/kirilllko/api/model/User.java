package ru.kirilllko.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Accessors(chain = true)
@Document(collection = "users")
public class User {

    @Id
    @ApiModelProperty(value = "Id", example = "123")
    private String userId;

    @ApiModelProperty(value = "Имя", example = "Кирилл")
    private String firstName;

    @ApiModelProperty(value = "Фамилия", example = "Зубков")
    private String lastName;

    @ApiModelProperty(value = "Возраст", example = "24")
    private int age;

    @ApiModelProperty(value = "Фото", example = "Absjwkw123==")
    private byte[] photo;
}