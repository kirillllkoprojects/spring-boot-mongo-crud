package ru.kirilllko.api.exception;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ExceptionComponent {
    private String field;
    private String message;
}
